package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.endpoint.*;
import ru.t1.aayakovlev.tm.endpoint.impl.*;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.SessionRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;
import ru.t1.aayakovlev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.PID_FILENAME;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final AuthEndpoint authEndpoint = new AuthEndpointImpl(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointImpl(this);

    @Getter
    @NotNull
    private final LoggerService loggerService = new LoggerServiceImpl();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final SystemEndpoint systemEndpoint = new SystemEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(connectionService);

    @Getter
    @NotNull
    private final SessionService sessionService = new SessionServiceImpl(connectionService);

    @Getter
    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(connectionService);

    @Getter
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(connectionService);

    @Getter
    @NotNull
    private final UserService userService = new UserServiceImpl(propertyService, connectionService);

    @Getter
    @NotNull
    private final AuthService authService = new AuthServiceImpl(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final DomainService domainService = new DomainServiceImpl(
            authService,
            projectService,
            taskService,
            userService
    );

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);

    }

    private void initAdmin() throws AbstractException {
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initBackup() {
        backup.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() throws AbstractException {
        initPID();
        initAdmin();
        initBackup();
        loggerService.info(
                "___________   _____      _________                                       \n" +
                "\\__    ___/  /     \\    /   _____/  ____  _______ ___  __  ____  _______ \n" +
                "  |    |    /  \\ /  \\   \\_____  \\ _/ __ \\ \\_  __ \\\\  \\/ /_/ __ \\ \\_  __ \\\n" +
                "  |    |   /    Y    \\  /        \\\\  ___/  |  | \\/ \\   / \\  ___/  |  | \\/\n" +
                "  |____|   \\____|__  / /_______  / \\___  > |__|     \\_/   \\___  > |__|   \n" +
                "                   \\/          \\/      \\/                     \\/         ");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        backup.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

}
