package ru.t1.aayakovlev.tm.repository.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.SessionRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepositoryImpl extends AbstractUserOwnedRepository<Session> implements SessionRepository {

    public SessionRepositoryImpl(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    private final static String TABLE_NAME = "public.session";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.toRole(row.getString("role")));
        session.setDate(row.getTimestamp("date"));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session save(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "insert into %s (id, date, user_id, role) values (?, ?, ?, ?)", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole() != null ? session.getRole().toString() : null);
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session save(@NotNull final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        save(session);
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "update %s set date = ?, role = ? where id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getRole() != null ? session.getRole().toString() : null);
            statement.executeUpdate();
        }
        return session;
    }

}
