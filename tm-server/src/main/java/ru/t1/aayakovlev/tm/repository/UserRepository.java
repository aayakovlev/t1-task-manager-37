package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.User;

public interface UserRepository extends BaseRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    boolean isEmailExists(@NotNull final String email);

    @NotNull
    User update(@NotNull final User user);

}
