package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.sql.Connection;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public final class ProjectServiceImpl extends AbstractUserOwnedService<Project> implements ProjectService {

    public ProjectServiceImpl(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepositoryImpl(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project project = findById(userId, id);
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull ProjectRepository repository = getRepository(connection);
            @NotNull final Project updatedProject = repository.update(project);
            connection.commit();
            return updatedProject;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull ProjectRepository repository = getRepository(connection);
            @NotNull final Project project = repository.create(userId, name);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull ProjectRepository repository = getRepository(connection);
            @NotNull final Project project = repository.create(userId, name, description);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull ProjectRepository repository = getRepository(connection);
            @NotNull final Project updatedProject = repository.update(project);
            connection.commit();
            return updatedProject;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
