package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionServiceImpl implements ConnectionService {

    @NotNull
    private final DatabaseProperty databaseProperty;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseURL();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
