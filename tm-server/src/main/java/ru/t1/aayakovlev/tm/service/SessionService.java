package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Session;

public interface SessionService extends UserOwnedService<Session> {

}
