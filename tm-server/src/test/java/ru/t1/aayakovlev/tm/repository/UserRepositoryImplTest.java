package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.UserServiceImpl;

import java.sql.Connection;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final UserRepository repository = new UserRepositoryImpl(connectionService.getConnection());

    @NotNull
    private static final UserService service = new UserServiceImpl(propertyService, connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(COMMON_USER_ONE);
        service.save(COMMON_USER_TWO);
    }

    @After
    public void after() {
        service.removeAll(USER_LIST);
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() throws AbstractException {
        @Nullable final User user = repository.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnNull() throws AbstractException {
        @Nullable final User user = repository.findById(USER_ID_NOT_EXISTED);
        Assert.assertNull(user);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullUser_Expect_ReturnUser() {
        @NotNull final User savedUser = repository.save(ADMIN_USER_THREE);
        connection.commit();
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(ADMIN_USER_THREE.getId(), savedUser.getId());
        @Nullable final User user = repository.findById(ADMIN_USER_THREE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_THREE.getId(), user.getId());
    }

    @Test
    public void When_ExistsByIdExistedUser_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedUser_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(USER_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedUser_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedUser_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(USER_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListUsers() {
        final List<User> users = repository.findAll();
        for (int i = 0; i < users.size(); i++) {
            Assert.assertEquals(COMMON_USER_LIST.get(i).getId(), users.get(i).getId());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedUser_Expect_ReturnUser() {
        @Nullable User user1 = repository.save(ADMIN_USER_ONE);
        connection.commit();
        Assert.assertNotNull(user1);
        @Nullable User user2 = repository.remove(ADMIN_USER_ONE);
        connection.commit();
        Assert.assertNotNull(user2);
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_NullUsers() {
        repository.save(ADMIN_USER_ONE);
        connection.commit();
        repository.save(ADMIN_USER_TWO);
        connection.commit();
        repository.removeAll(ADMIN_USER_LIST);
        connection.commit();
        Assert.assertNull(repository.findById(ADMIN_USER_ONE.getId()));
        Assert.assertNull(repository.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedUser_Expect_User() {
        @Nullable User user1 = repository.save(ADMIN_USER_ONE);
        connection.commit();
        Assert.assertNotNull(user1);
        @Nullable User user2 = repository.removeById(ADMIN_USER_ONE.getId());
        connection.commit();
        Assert.assertNotNull(user2);
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(USER_ID_NOT_EXISTED)
        );
    }

}
