package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.TaskServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class TaskServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private final TaskRepository repository = new TaskRepositoryImpl(connectionService.getConnection());

    @NotNull
    private static final TaskService service = new TaskServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        service.save(TASK_USER_ONE);
        service.save(TASK_USER_TWO);
    }

    @AfterClass
    public static void after() {
        service.removeAll(TASK_LIST);
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
        @Nullable final Task task = service.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(TASK_ID_NOT_EXISTED));

    }

    @Test
    public void When_SaveNotNullProject_Expect_ReturnProject() throws AbstractException {
        @NotNull final Task savedTask = service.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE);
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(TASK_ADMIN_ONE, savedTask);
        @Nullable final Task task = service.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE, task);
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() throws UserIdEmptyException {
        final List<Task> tasks = service.findAll(COMMON_USER_ONE.getId());
        Assert.assertArrayEquals(tasks.toArray(), USER_TASK_LIST.toArray());
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() throws UserIdEmptyException {
        @NotNull final List<Task> tasks = service.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME.getComparator());
        Assert.assertArrayEquals(tasks.toArray(), USER_TASK_SORTED_LIST.toArray());
    }

    @Test
    public void When_FindAllByProjectId_Expect_ReturnTasks() throws AbstractFieldException {
        Assert.assertEquals(1, service.findByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId()).size());
    }

    @Test
    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
        Assert.assertNotNull(service.remove(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.remove(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED)
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountProjects() throws AbstractException {
        service.save(TASK_ADMIN_ONE);
        service.save(TASK_ADMIN_TWO);
        service.removeAll(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedProject_Expect_Project() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
        Assert.assertNotNull(service.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_CreateNameProject_Expect_ExistedProject() throws AbstractFieldException {
        @Nullable final Task task = service.create(ADMIN_USER_ONE.getId(), NAME);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
    }

    @Test
    public void When_CreateNameDescriptionProject_Expect_ExistedProject() throws AbstractFieldException {
        @Nullable final Task task = service.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

    @Test
    public void When_UpdateByIdProject_Expect_UpdatedProject() throws AbstractException {
        service.save(TASK_ADMIN_TWO);
        @Nullable Task task = service.updateById(
                TASK_ADMIN_TWO.getUserId(), TASK_ADMIN_TWO.getId(), NAME, DESCRIPTION
        );
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

}
