package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataJsonLoadJaxBRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-jaxb-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().jsonLoadJaxB(new DataJsonLoadJaxBRequest(getToken()));
    }

}
