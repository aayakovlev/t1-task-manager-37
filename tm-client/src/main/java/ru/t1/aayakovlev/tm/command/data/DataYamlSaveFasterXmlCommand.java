package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to yaml file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE YAML]");
        getDomainEndpoint().yamlSave(new DataYamlSaveFasterXmlRequest(getToken()));
    }

}
